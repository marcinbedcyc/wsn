# Wstęp do sieci neuronowych - laboratorium

### Opis
Projekt z zadaniami z przedmiotu Wstęp do Sieci Neuronowych(laboratorium).

### Uruchomienie lokalnie
1. Utwórz środowisko wirtualne przy pomocy venv/mkvritualenv:
    ```
    mkvirtualnenv wsn
    ````
1. Zaktualizuj pip:
    ```
    pip install --upgrade pip
    ````
1. Zainstaluj wymagania:
    ```
    pip install -r requirements/requirements.txt
    ```
1. Zainstaluj moduły w środowisku wirtualnym:
    ```
    pip install -e .
    ```
1. Uruchom odpowiednie zadanie (należy znajdować się w katalogu `wsn/zadania`):
    ```
    zadanie1.py
    ```
1. Alternatywnie można zrezygnować z instalacji modułów i uruchamiać interesujące nas zadania poprzez (również w środowisku wirtualnym):
    ```
    python zadanie1.py
    ```

### Zadania
1. Program, który wykorzystuje 10 **perceptronów** do rozpoznawania cyfr.
2. Program, który wykorzystuje 10 **perceptronów Adaline** do rozpoznawania cyfr.
3. Program, który wykorzystuje wsteczną propagację błędu do utworzenia sieci neuronowej służącej do przewidywania kątów zgięcia ramienia robota.

### Opis modułów
* **wsn.data** - dane wykorzystywane w procesie uczenia.
* **wsn.neural_networks** -  implementacja jednostek uczących takich jak Perceptron czy Adaline.
* **wsn.gui** - iterfejsy graficzne wykorzystywane w zadaniach
* **zadania** - skrypty z zadaniami.
* **plots** - wykresy generowane podczas uczenia sieci np. wykres funkcji błędu podczas uczenia adaline.