import json
import matplotlib.pyplot as plt
import numpy as np


ARM_LENGTH = 100


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y


class Examples:
    def __init__(self, arm_length=10):
        self.arm_length = arm_length
        self.center = Point(0, 0)
        self.input = []
        self.temp_points = []
        self.output = []

    def generate(self, number_of_examples):
        for _ in range(number_of_examples):
            point, temp_point = self.generate_point()
            self.input.append([point.x, point.y])
            self.temp_points.append([temp_point.x, temp_point.y])
        return self.input, self.output, self.temp_points

    def generate_point(self):
        alpha = np.random.random() * np.pi
        beta = np.random.random() * np.pi
        self.output.append([alpha, beta])
        temp_point = self.translate(self.center, alpha)
        final_point = self.translate(temp_point, np.pi - beta + alpha)
        return final_point, temp_point

    def translate(self, center, angle):
        return Point(
            center.x + self.arm_length * np.sin(angle),
            center.y - self.arm_length * np.cos(angle)
        )


def get_temp_point_and_final(alpha, beta, center, arm_length):
    temp_point = translate(center, alpha, arm_length)
    final_point = translate(temp_point, np.pi - beta + alpha, arm_length)
    return final_point, temp_point


def translate(center, angle, arm_length):
    return Point(
        center.x + arm_length * np.sin(angle),
        center.y - arm_length * np.cos(angle)
    )


def make_data(count=1500):
    # Data generation
    examples = Examples(arm_length=ARM_LENGTH)
    inputs, outputs, temp_points = examples.generate(count)

    # Scale x of points
    inputs = np.array(inputs)
    # inputs /= (examples.arm_length * 2)
    min = inputs[:, 0].min()
    max = inputs[:, 0].max()
    for i in range(len(inputs)):
        inputs[i][0] = 0 + (inputs[i][0] - min)/(max - min) * 200

    temp_points = np.array(temp_points)
    min = temp_points[:, 0].min()
    max = temp_points[:, 0].max()
    for i in range(len(temp_points)):
        temp_points[i][0] = 0 + (temp_points[i][0] - min)/(max - min) * 200

    for i in range(len(outputs)):
        outputs[i][0] = 0.8 * outputs[i][0] / np.pi + 0.1
        outputs[i][1] = 0.8 * outputs[i][1] / np.pi + 0.1

    data_dict = {
        'inputs': inputs.tolist(),
        'outputs': outputs,
        'temp_points': temp_points.tolist()
    }
    with open('robot_arm_points.json', 'w')as json_file:
        json.dump(data_dict, json_file)


def main():
    make_data()
    with open('robot_arm_points.json') as jsonfile:
        data = json.load(jsonfile)
        inputs = data['inputs']
        # temp_points = data['temp_points']

    # Drawing plots
    fig, ax = plt.subplots()
    # ax.plot('equals')
    for (x, y) in inputs:
        plt.scatter(x, y, marker='o')
    plt.show()


if __name__ == "__main__":
    main()
