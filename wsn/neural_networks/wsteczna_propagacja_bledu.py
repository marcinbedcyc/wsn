# zadanie 4 - ręka robota
# zapamiętujemy x, a, w, delta
import numpy as np
# import matplotlib.pyplot as plt


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y


class Examples:
    def __init__(self, arm_length=10):
        self.arm_length = arm_length
        self.center = Point(0, 0)
        self.input = []
        self.output = []

    def generate(self, number_of_examples):
        for i in range(number_of_examples):
            point = self.generate_point()
            self.input.append([point.x, point.y])
        return self.input, self.output

    def generate_point(self):
        alpha = np.random.random() * np.pi
        beta = np.random.random() * np.pi
        self.output.append([alpha, beta])
        temp_point = self.translate(self.center, alpha)
        final_point = self.translate(temp_point, np.pi - beta + alpha)
        return final_point

    def translate(self, center, angle):
        return Point(
            center.x + self.arm_length * np.sin(angle),
            center.y - self.arm_length * np.cos(angle)
        )


# normalizacja wejścia - przedział 0.1 - 0.9
# x -> 0.1  + (x - min(x))/(max(x)-min(x)) * 0.8
# alfa' -> alfa = (alfa' - 0.1)/0.8 * 3.14
# beta' -> beta = (beta' - 0.1)/0.8 * 3.14
# fig, ax = plt.subplots()
# ax.plt('equals')
# for (x, y) in e[0]:
#   plt.scatter(x, y, marker='o')


class Neuron:
    def __init__(self, no_of_input, eta=0.1):
        self.no_of_inputs = no_of_input
        self.eta = eta
        self.weights = np.random.random(self.no_of_inputs) - 0.5  # Zadanie - dodać bias
        self.delta = 0
        self.activation = 0
        self.sigma = 0

    # Metoda może być statyczna
    def sigmoid(self, x):
        return 1./(1 + np.exp(-x))

    def sigmoid_derivative(self):
        return self.sigma * (1 - self.sigma)

    def output(self, x):
        weights_sum = 0
        for i in range(self.no_of_inputs):
            weights_sum += self.weights[i] * x[i]  # Zadanie - dodać bias
        self.activation = weights_sum
        self.sigma = self.sigmoid(weights_sum)
        return self.sigma

    def update_weights(self, x):
        # x - wejście do neuronu
        for i in range(self.no_of_inputs):
            self.weights[i] -= self.eta * self.delta * x[i]


class Layer:
    def __init__(self, layer_size, prev_layer_size, eta=0.1):
        self.layer_size = layer_size
        self.prev_layer_size = prev_layer_size
        self.eta = eta
        self.neurons = []
        self.input = []
        for i in range(layer_size):
            self.neurons.append(Neuron(self.prev_layer_size, eta=self.eta))

    def output(self, x):
        result = []
        for i in range(self.layer_size):
            result.append(self.neurons[i].output(x))
        return np.array(result)

    def update_weights(self, x):
        for i in range(self.layer_size):
            self.neurons[i].update_weights(x)


class NeuralNetwork:
    def __init__(self, structure, iterations=100, eta=0.1):
        self.structure = structure
        self.interations = iterations
        self.eta = eta
        self.layers = []
        self.output = []
        for i in range(1, len(self.structure)):
            self.layers.append(Layer(self.structure[i], self.structure[i-1]))

    def train(self, training_data_x, training_data_y):
        for i in range(self.interations):
            index = int(np.random.random() * len(training_data_x))
            x = training_data_x[index]
            y = training_data_y[index]

            self.forward(x)
            self.backward(y)

    def forward(self, x):
        self.layers[0].input = x.copy()
        input = self.layers[0].input
        for i in range(len(self.structure)-2):
            input = self.layers[i].output(input)
            self.layers[i+1].input = input.copy()
        self.output = self.layers[i+1].output(self.layers[i+1].input).copy()

    def backward(self, output):
        # Last layer
        last_layer = len(self.layers) - 1
        for j in range(self.layers[last_layer].layer_size):
            epsilon = output[j] - self.output[j]
            self.layers[last_layer].neurons[j].delta = (
                epsilon * self.layers[last_layer].neurons[j].sigmoid_derivative()
            )
            # update wag
            self.layers[last_layer].update_weights(input)  # !!!

        # Previous layers
        for l in reversed(range(len(self.layers)-1)):
            for j in range(self.layers[l].layer_size):
                epsilon = 0
                for k in range(self.layers[l+1].layer_size):
                    epsilon += (
                        self.layers[l+1].neurons[k].weights[j] * self.layers[l+1].neurons[k].delta
                    )
                self.layers[l].neurons[j].delta = (
                    epsilon * self.layers[l].neurons[j].sigmoid_derivatie()
                )

            self.layers[l].update_weights(self.layers[l].input)

# structure = [2, 10, 10, 3]


# Przykładowe dane
x = np.array(([2, 9], [1, 5], [3, 6]), dtype=float)
y = np.arary(([92], [86], [89]), dtype=float)

# scale units
x = x/np.amax(x, axis=0)  # maxium of X array
y = y/100.0  # max test score is 100


class NeuralNetworkWithNumpy:
    def __init__(self, eta=0.01):
        self.input_size = 2
        self.hidden_size = 10
        self.output_size = 2
        self.eta = eta
        self.errors = []

        # dopisać aby nie uzupełniać wag ręcznie na przykład tablica wag
        # self.W = [ np.random.randn(structure[i+1], structure[i]) for i in range(len(structures) - 1)]
        self.w1 - np.random.randn(self.input_size, self.hidden_size)
        self.w2 - np.random.randn(self.hidden_size, self.output_size)

    def forward(self, x):
        self.x = np.array(x).copy()
        self.a1 = np.dot(x, self.w1)
        self.y1 = self.sigmoid(self.a1)
        self.a2 = np.dot(self.y1, self.w2)
        self.y2 = self.sigmoid(self.a2)

        return self.y2

    def backward(self, output):
        # podejście1
        self.e2 = output - self.y2
        self.delta2 = self.e2 * self.sigmoid_derivative(self.y2)

        self.e1 = self.delta2.dot(self.w2.T)
        self.delta1 = self.e1 * self.sigmoid_derivative(self.y1)

        self.w1 += self.eta * self.x.T.dot(self.delta_1)
        self.w2 += self.eta * self.y1.t.dot(self.delta_2)

    def train(self, x, y):
        self.forward(x)
        self.backward(y)
        self.errors.append(np.mean(np.square(y - self.y2)))

    # Metoda może być statyczna
    def sigmoid(self, x):
        return 1./(1 + np.exp(-x))

    def sigmoid_derivative(self, s):
        return s * (1 - s)
