import numpy as np


def sigmoid(x):
    return 1./(1 + np.exp(-x))


def sigmoid_derivative(s):
    return s * (1 - s)


class BackpropagationNeuralNetwork:
    def __init__(self, network_structure, eta=0.01, weights=None, bias=None):
        # Array of neurons number in each layer (included input and output)
        # eg. (2, 5, 5, 5, 2)
        self.network_structure = network_structure
        self.layers_number = len(self.network_structure)
        self.eta = eta
        self.y = list(range(self.layers_number))
        self.a = list(range(self.layers_number))  # y value before activation
        self.epsilon = list(range(self.layers_number))
        self.delta = list(range(self.layers_number))
        self.weights = list(range(self.layers_number))
        self.bias = list(range(self.layers_number))

        if not weights and not bias:
            for i in range(self.layers_number-1):
                self.weights[i+1] = np.random.uniform(
                    -0.5, 0.5, (network_structure[i], network_structure[i+1])
                )
                self.bias[i+1] = np.random.uniform(
                    -0.5, 0.5, network_structure[i+1]
                )
        else:
            self.weights = weights
            self.bias = bias

    def forward(self, x):
        self.y[0] = np.array(x).copy()

        for i in range(1, self.layers_number):
            self.a[i] = np.dot(self.y[i-1], self.weights[i]) + self.bias[i]
            self.y[i] = sigmoid(self.a[i])

        return self.y[self.layers_number-1]

    def backward(self, output):
        self.delta[-1] = (self.y[-1] - output) * sigmoid_derivative(self.y[-1])

        for i in reversed(range(1, self.layers_number - 1)):
            self.epsilon[i] = self.delta[i+1].dot(self.weights[i+1].T)
            self.delta[i] = self.epsilon[i] * sigmoid_derivative(self.y[i])

        for i in range(1, self.layers_number):
            self.weights[i] -= self.eta * np.dot(
                self.y[i-1].reshape(len(self.y[i-1]), 1),
                self.delta[i].reshape(1, len(self.delta[i]))
            )
            self.bias[i] -= self.eta * self.delta[i]

    def train(self, x, y):
        self.forward(x)
        self.backward(y)

    def predict(self, x, debug=True):
        alfa, beta = self.forward(x)
        if debug:
            print(f"Before {(alfa, beta)}")
        # Undo normalize
        alfa = (alfa - 0.1) * np.pi / 0.8
        beta = (beta - 0.1) * np.pi / 0.8
        return alfa, beta
