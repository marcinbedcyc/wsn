import random

import numpy as np
import matplotlib.pyplot as plt


def fourier_transform(x):
    a = np.abs(np.fft.fft(x))
    a[0] = 0  # domyślnie jest suma wartości wektora x
    return a/np.amax(a)
    # return np.abs(np.fft.fft(x))


class Adaline:
    def __init__(
        self,
        no_inputs,
        learning_rate=0.01,
        iterations=100,
        activation=lambda x: x,
        activation_derivative=lambda x: 1,
        error_limit=0.1
    ):
        self.no_inputs = no_inputs
        self.learning_rate = learning_rate
        self.iterations = iterations
        self.activation = activation
        self.act_derivative = activation_derivative
        self.error_limit = error_limit
        self.weights = np.random.default_rng().uniform(-0.5, 0.5, 2*self.no_inputs + 1)
        self.errors = []

    def train(self, training_data, labels, name, with_normalize=False, with_standarize=False):
        training_data_count = len(training_data)
        flatten_training_data = [x.flatten() for x in training_data]

        for _ in range(self.iterations):
            random_data_index = random.randint(0, training_data_count-1)
            x = flatten_training_data[random_data_index]
            label = labels[random_data_index]

            x = np.concatenate([x, fourier_transform(x)])
            if with_standarize:
                x = self._standarize(x)
            if with_normalize:
                x = self._normalize(x)
            out = self.output(x)

            self.weights[1:] += self.learning_rate * (label - out) * x * self.act_derivative(out)
            self.weights[0] += self.learning_rate * (label - out) * self.act_derivative(out)

            error = self._calculate_error(flatten_training_data, labels)
            self.errors.append(error)
            if error < self.error_limit:
                break

        self._make_error_plot(name)

    def _standarize(self, data):
        """Standarize output wejść  x' = (x - mean(x))/std(x)"""
        return (data - np.mean(data)) / np.std(data)

    def _normalize(self, data):
        """Normalize output  x' = (x - min(x)) / (max(x) - min(x))"""
        return (data - np.min(data)) / (np.max(data) - np.min(data))

    def output(self, input):
        sum = np.dot(self.weights[1:], input) + self.weights[0]
        return self.activation(sum)

    def _make_error_plot(self, name, dir_path="plots"):
        """Making error function plot"""
        plt.plot(range(len(self.errors)), self.errors)
        plt.xlabel("Iterations")
        plt.ylabel("Error")
        plt.grid(True)
        plt.title(f"Learning error for number {name}")
        plt.savefig(f'{dir_path}/learning_errors_{name}.pdf')
        plt.cla()

    def _calculate_error(self, training_data, labels):
        """Calculate error function based on all learning examples"""
        error = 0
        for data, label in zip(training_data, labels):
            data = np.concatenate([data, fourier_transform(data)])
            out = self.output(data)
            error += (label - out) ** 2  # Optional multiple 0.5
        return error
