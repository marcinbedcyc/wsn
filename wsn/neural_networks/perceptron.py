import random
import numpy as np


class Perceptron:
    def __init__(self, no_of_inputs, learning_rate=0.01, interations=10000):
        self.interations = interations
        self.learning_rate = learning_rate
        self.no_of_inputs = no_of_inputs
        self.activation_func = lambda value: 1 if value > 0 else 0
        self._init_values()

    def spla_train(self, training_data, labels):
        """Simple Perceptron Learing Algorithm"""
        self._init_values()
        training_data_count = len(training_data)
        training_data = self._prepare_data(training_data)
        zipped_data_and_labels_list = list(zip(training_data, labels))

        for _ in range(self.interations):
            guard_index = 0  # Used in stop condition
            # Make permutation of data and labels. A bit slow solutiona for big data.
            random.shuffle(zipped_data_and_labels_list)
            for input, label in zipped_data_and_labels_list:
                guard_index += 1
                prediction = self.predict(input)
                err = label - prediction
                if err != 0:
                    self.weights[1:] += self.learning_rate * err * input
                    self.weights[0] += self.learning_rate * err
                    break  # Take next iteration
            # Stop condition if nested loop and without break.
            if guard_index == training_data_count:
                break

    def pla_train(self, training_data, labels):
        """Pocket Learning Algorithm"""
        self._init_values()
        training_data_count = len(training_data)
        training_data = self._prepare_data(training_data)
        local_tl = 0
        local_weights = self.weights

        for _ in range(self.interations):
            random_data_index = random.randint(0, training_data_count-1)
            data = training_data[random_data_index]
            prediction = self.predict(data)
            err = labels[random_data_index] - prediction
            if err != 0:
                local_weights[1:] += self.learning_rate * err * data
                local_weights[0] += self.learning_rate * err
                local_tl = 0
            else:
                local_tl += 1
                if local_tl > self.best_weights_tl:
                    self.best_weights_tl = local_tl
                    self.weights = local_weights

    def pla_with_ratchet_train(self, training_data, labels):
        """Pocket Learning Algorithm with Ratchet"""
        self._init_values()
        training_data_count = len(training_data)
        training_data = self._prepare_data(training_data)
        local_tl = 0
        local_classified_counter = 0
        classified = [False] * training_data_count
        local_weights = self.weights

        for _ in range(self.interations):
            random_data_index = random.randint(0, training_data_count-1)
            data = training_data[random_data_index]
            prediction = self.predict(data)
            err = labels[random_data_index] - prediction
            if err != 0:
                local_weights[1:] += self.learning_rate * err * data
                local_weights[0] += self.learning_rate * err
                local_tl = 0
                local_classified_counter = 0
                classified = [False] * training_data_count
            else:
                local_tl += 1
                if not classified[random_data_index]:
                    classified[random_data_index] = True
                    local_classified_counter += 1
                if(
                    local_tl > self.best_weights_tl
                    and local_classified_counter > self.best_classified_counter
                ):
                    self.best_weights_tl = local_tl
                    self.best_classified_counter = local_classified_counter
                    self.weights = local_weights

    def predict(self, input):
        """Make prediction for data is classified or not.

        Args:
            input (list): Flatten data for classification e.g array 5x7 should be passed as
                array 35x1

        Returns:
            int: 1 if data is classified else 0.
        """
        return self.activation_func(np.dot(input, self.weights[1:]) + self.weights[0])

    def _prepare_data(self, training_data):
        prepared_data = [None] * len(training_data)
        for index, data in enumerate(training_data):
            prepared_data[index] = data.flatten()
        return prepared_data

    def _init_values(self):
        """Values(best weights' timelife, classified counter and random weights) setup initialization.
        """
        self.best_weights_tl = 0
        self.best_classified_counter = 0
        self.weights = np.random.default_rng().uniform(-0.5, 0.5, self.no_of_inputs + 1)
