import tkinter as tk

import matplotlib.pyplot as plt
import numpy as np
from PIL import ImageTk, Image

from wsn.data.numbers_data import numbers_5x7, numbers_9x9  # , numbers_7x7,


CLICKED_COLOR = '#2B4162'  # 1
NON_CLICKED_COLOR = '#D7B377'  # 0


class PerceptronGUI(tk.Frame):
    def __init__(self, neural_network, master=None, rows=5, cols=7):
        super().__init__(master)
        self.master = master
        self.rows_n = rows
        self.cols_n = cols
        self.neural_network = neural_network
        self.init_components()

    def init_widgets(self):
        for i in range(self.rows_n):
            row = []
            for j in range(self.cols_n):
                button = tk.Button(
                    self,
                    height=4,
                    width=7,
                    background=NON_CLICKED_COLOR,
                    command=lambda i=i, j=j: self.button_click(i, j)
                )
                row.append(button)
                button.grid(row=i, column=j)
            self.buttons.append(row)

        # negative button
        self.negative_button = tk.Button(self, text="NEG", width=7, command=self.negative)
        self.negative_button.grid(row=self.rows_n+1, column=0)

        # learn button
        self.learn_button = tk.Button(self, text="LEARN", width=7, command=self.learn)
        self.learn_button.grid(row=self.rows_n+1, column=1)

        # one button
        self.one_button = tk.Button(self, text="ONE", width=7, command=self.one)
        self.one_button.grid(row=self.rows_n+1, column=2)

        # two button
        self.two_button = tk.Button(self, text="TWO", width=7, command=self.two)
        self.two_button.grid(row=self.rows_n+1, column=3)

        # three button
        self.three_button = tk.Button(self, text="THREE", width=7, command=self.three)
        self.three_button.grid(row=self.rows_n+1, column=4)

        # four button
        self.four_button = tk.Button(self, text="FOUR", width=7, command=self.four)
        self.four_button.grid(row=self.rows_n+1, column=5)

        # five button
        self.five_button = tk.Button(self, text="FIVE", width=7, command=self.five)
        self.five_button.grid(row=self.rows_n+1, column=6)

        # six button
        self.six_button = tk.Button(self, text="SIX", width=7, command=self.six)
        self.six_button.grid(row=self.rows_n+2, column=0)

        # seven button
        self.seven_button = tk.Button(self, text="SEVEN", width=7, command=self.seven)
        self.seven_button.grid(row=self.rows_n+2, column=1)

        # eight button
        self.eight_button = tk.Button(self, text="EIGHT", width=7, command=self.eight)
        self.eight_button.grid(row=self.rows_n+2, column=2)

        # nine button
        self.nine_button = tk.Button(self, text="NINE", width=7, command=self.nine)
        self.nine_button.grid(row=self.rows_n+2, column=3)

        # zero button
        self.zero_button = tk.Button(self, text="ZERO", width=7, command=self.zero)
        self.zero_button.grid(row=self.rows_n+2, column=4)

        # random button
        self.clear_button = tk.Button(self, text="RAND", width=7, command=self.random_array)
        self.clear_button.grid(row=self.rows_n+2, column=5)

        # vertical mirror button
        self.ver_button = tk.Button(self, text="VER", width=7, command=self.mirror_vertical)
        self.ver_button.grid(row=self.rows_n+2, column=6)

        # horizontal mirror button
        self.ver_button = tk.Button(self, text="HOR", width=7, command=self.mirror_horizontal)
        self.ver_button.grid(row=self.rows_n+3, column=0)

        # move left
        self.left_button = tk.Button(self, text="<<", width=7, command=self.move_left)
        self.left_button.grid(row=self.rows_n+3, column=1)

        # move right
        self.ver_button = tk.Button(self, text=">>", width=7, command=self.move_right)
        self.ver_button.grid(row=self.rows_n+3, column=2)

        # move up
        self.ver_button = tk.Button(self, text="^", width=7, command=self.move_up)
        self.ver_button.grid(row=self.rows_n+3, column=3)

        # move down
        self.ver_button = tk.Button(self, text="v", width=7, command=self.move_down)
        self.ver_button.grid(row=self.rows_n+3, column=4)

        # clear button
        self.clear_button = tk.Button(self, text="CLEAR", width=7, command=self.clear)
        self.clear_button.grid(row=self.rows_n+3, column=5)

        # Exit button
        self.exit_button = tk.Button(
            self,
            text="QUIT",
            command=self.exit_click,
            width=7
        )
        self.exit_button.grid(row=self.rows_n+4, column=0)

        # predict button
        self.predict_button = tk.Button(self, text="PREDICT", command=self.predict, width=7)
        self.predict_button.grid(row=self.rows_n+4, column=1)

        # out label
        self.out_label = tk.Label(self, text="OUT:")
        self.out_label.grid(row=self.rows_n+4, column=2)

        # result label
        self.result_label = tk.Label(self, text="")
        self.result_label.grid(row=self.rows_n+4, column=3, columnspan=2)

    def exit_click(self):
        self.master.destroy()

    def learn(self):
        print("Learn button clicked")
        self.neural_network.learn()

    def predict(self):
        print("Predict button clicked")
        value = self.neural_network.predict(self.number_array.flatten())
        self.result_label.config(text=value)

    def button_click(self, i, j):
        print(f'Clicked button ({i}, {j})\n', self.number_array)
        button = self.buttons[i][j]
        if self.number_array[i][j] == 0.0:
            button.configure(background=CLICKED_COLOR)
            self.number_array[i][j] = 1.0
        else:
            button.configure(background=NON_CLICKED_COLOR)
            self.number_array[i][j] = 0.0

    def clear(self):
        print('Clicked clear button')
        for i in range(self.rows_n):
            for j in range(self.cols_n):
                self.buttons[i][j].configure(background=NON_CLICKED_COLOR)
                self.number_array[i][j] = 0.0

    def negative(self):
        print('Clicked negative button')
        for i in range(self.rows_n):
            for j in range(self.cols_n):
                if self.number_array[i][j] == 0.0:
                    self.buttons[i][j].configure(background=CLICKED_COLOR)
                    self.number_array[i][j] = 1.0
                else:
                    self.buttons[i][j].configure(background=NON_CLICKED_COLOR)
                    self.number_array[i][j] = 0.0

    def move_up(self):
        print('Clicked move up button')
        first_row = self.number_array[0]
        self.number_array = np.delete(self.number_array, 0, 0)
        self.number_array = np.vstack((self.number_array, first_row))
        self.color_buttons()

    def move_down(self):
        print('Clicked move up button')
        last_row = self.number_array[-1]
        self.number_array = np.delete(self.number_array, -1, 0)
        self.number_array = np.insert(self.number_array, 0, last_row, axis=0)
        self.color_buttons()

    def move_left(self):
        print('Clicked move left button')
        first_column = np.reshape(self.number_array[:, 0], (self.rows_n, 1))
        self.number_array = np.delete(self.number_array, 0, 1)
        self.number_array = np.hstack((self.number_array, first_column))
        self.color_buttons()

    def move_right(self):
        print('Clicked move right button')
        last_row = self.number_array[:, -1]
        self.number_array = np.delete(self.number_array, -1, 1)
        self.number_array = np.insert(self.number_array, 0, last_row, axis=1)
        self.color_buttons()

    def mirror_horizontal(self):
        print("Mirror horizontally button clicked")
        self.number_array = np.flipud(self.number_array)
        self.color_buttons()

    def mirror_vertical(self):
        print("Mirror veritcally button clicked")
        self.number_array = np.fliplr(self.number_array)
        self.color_buttons()

    def random_array(self):
        print("Randomize number button clicked")
        self.number_array = np.random.randint(0, 2, (self.rows_n, self.cols_n)).astype('float')
        self.color_buttons()

    def one(self):
        print("One button clicked")
        self.put_number_in_gui(1)

    def two(self):
        print("Two button clicked")
        self.put_number_in_gui(2)

    def three(self):
        print("Three button clicked")
        self.put_number_in_gui(3)

    def four(self):
        print("Four button clicked")
        self.put_number_in_gui(4)

    def five(self):
        print("Five button clicked")
        self.put_number_in_gui(5)

    def six(self):
        print("Six button clicked")
        self.put_number_in_gui(6)

    def seven(self):
        print("Seven button clicked")
        self.put_number_in_gui(7)

    def eight(self):
        print("Eight button clicked")
        self.put_number_in_gui(8)

    def nine(self):
        print("Nine button clicked")
        self.put_number_in_gui(9)

    def zero(self):
        print("Zero button clicked")
        self.put_number_in_gui(0)

    def color_buttons(self):
        for i in range(self.rows_n):
            for j in range(self.cols_n):
                if self.number_array[i][j] == 0.0:
                    self.buttons[i][j].configure(background=NON_CLICKED_COLOR)
                else:
                    self.buttons[i][j].configure(background=CLICKED_COLOR)

    def put_number_in_gui(self, number):
        num = numbers_5x7[number*10 + 1]
        self.number_array = np.zeros((self.rows_n, self.cols_n))
        self.number_array[0:num.shape[0], 0:num.shape[1]] += num
        self.color_buttons()

    def init_components(self):
        self.grid()
        self.buttons = []
        self.number_array = np.zeros((self.rows_n, self.cols_n), dtype=np.float)
        self.init_widgets()


class AdalineGUI(PerceptronGUI):
    def __init__(self, neural_network, master=None, rows=5, cols=7, with_confidence=False):
        self.with_confidence = with_confidence
        super().__init__(neural_network, master, rows, cols)

    def init_widgets(self):
        super().init_widgets()
        # self.out_label.grid_forget()
        # self.result_label.grid_forget()

        self.predict_button.grid(row=self.rows_n+3, column=6)

        # rotate left button
        self.rotate_left_button = tk.Button(self, text="⤴️", width=7, command=self.rotate_left)
        self.rotate_left_button.grid(row=self.rows_n+4, column=1)

        # rotate right button
        self.rotate_right_button = tk.Button(self, text="⤵️", width=7, command=self.rotate_right)
        self.rotate_right_button.grid(row=self.rows_n+4, column=2)

        # transposition button
        self.transposition_button = tk.Button(
            self, text="TRANS", width=7, command=self.transposition
        )
        self.transposition_button.grid(row=self.rows_n+4, column=3)

        # Change posistio of result labels
        self.out_label.grid(row=self.rows_n+4, column=4)
        self.result_label.grid(row=self.rows_n+4, column=5, columnspan=2)

        if self.with_confidence:
            self.image_label = tk.Label(self, image=None)
            self.image_label.image = None
            self.image_label.grid(row=1, column=self.cols_n, rowspan=7, columnspan=7)

            self.confidence_label = tk.Label(self, text="CONFIDENCE:")
            self.confidence_label.grid(row=0, column=self.cols_n, columnspan=2)

    def put_number_in_gui(self, number):
        # num = numbers_7x7[number]
        num = numbers_9x9[number]
        self.number_array = np.zeros((self.rows_n, self.cols_n))
        self.number_array[0:num.shape[0], 0:num.shape[1]] += num
        self.color_buttons()

    def rotate_left(self):
        self.number_array = np.rot90(self.number_array, 1)
        self.color_buttons()

    def rotate_right(self):
        self.number_array = np.rot90(self.number_array, 3)
        self.color_buttons()

    def transposition(self):
        self.number_array = np.transpose(self.number_array)
        self.color_buttons()

    def exit_click(self):
        plt.close()  # Close all matplot figures
        super().exit_click()

    def predict(self):
        super().predict()
        if self.with_confidence:
            img = ImageTk.PhotoImage(
                Image.open(
                    "/home/marcin/Dokumenty/studia/SemestrVII/WSN/lab/wsn/plots/prediction.png"
                )
            )
            self.image_label.image = img
            self.image_label.configure(image=img)
