import os
import json
import tkinter as tk
from PIL import ImageTk, Image
from wsn.data.robot_points import get_temp_point_and_final, Point
from wsn.neural_networks.backpropagation import BackpropagationNeuralNetwork


MIDDLE_Y = 278
ARM_LENGTH = 100
WEIGHTS_FILE_PATH = 'weights/test.json'
STRUCTURE = (2, 10, 10, 2)


class RobotGUI(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        with open(WEIGHTS_FILE_PATH) as json_file:
            data = json.load(json_file)
            self.NN = BackpropagationNeuralNetwork(
                STRUCTURE,
                eta=0.005,
                weights=data['weights'],
                bias=data['bias']
            )

        cwd = os.getcwd()
        image_path = f'{cwd}/images/robot.png'
        if not os.path.exists(image_path):
            raise Exception(f'Nie istnieje {image_path}')
        img = ImageTk.PhotoImage(Image.open(image_path))
        self.robot_label = tk.Label(master, image=img)
        self.robot_label.image = img
        self.robot_label.grid(row=0, column=0)

        self.canvas = tk.Canvas(width=img.width(), height=img.height())
        self.canvas.bind("<Button-1>", self.callback)
        self.canvas.grid(row=0, column=1)

    def callback(self, event):
        print("Clicked at", event.x, event.y)
        self.canvas.delete("all")
        python_green = "#476042"
        x1, y1 = (event.x - 5), (event.y - 5)
        x2, y2 = (event.x + 5), (event.y + 5)
        self.canvas.create_oval(x1, y1, x2, y2, fill=python_green)

        # prznoszę x, y w układ kartezjański
        x = event.x
        y = -1 * event.y + MIDDLE_Y
        print(f"x: {x}, y: {y}")

        # make prediction:
        alfa, beta = self.NN.predict([x, y])
        print(f"alfa: {alfa}, beta: {beta}")

        # draw temp point and final in Cartesian system
        final_point, temp_point = get_temp_point_and_final(alfa, beta, Point(0, 0), ARM_LENGTH)
        print(f"FP: x:{final_point.x} y:{final_point.y}")
        print(f"TP: x:{temp_point.x} y:{temp_point.y}")

        # Move point to picture
        final_point.y = -1 * (final_point.y - MIDDLE_Y)
        temp_point.y = -1 * (temp_point.y - MIDDLE_Y)
        print(f"FP: x:{final_point.x} y:{final_point.y}")
        print(f"TP: x:{temp_point.x} y:{temp_point.y}")
        x1, y1 = (final_point.x - 5), (final_point.y - 5)
        x2, y2 = (final_point.x + 5), (final_point.y + 5)
        self.canvas.create_oval(x1, y1, x2, y2, fill='#FF0000')
        x1, y1 = (temp_point.x - 5), (temp_point.y - 5)
        x2, y2 = (temp_point.x + 5), (temp_point.y + 5)
        self.canvas.create_oval(x1, y1, x2, y2, fill='#0000FF')

        # Draw line between points
        self.canvas.create_line((0, MIDDLE_Y, temp_point.x, temp_point.y), fill="#000000")
        self.canvas.create_line(
            (temp_point.x, temp_point.y, final_point.x, final_point.y),
            fill="#000000"
        )
