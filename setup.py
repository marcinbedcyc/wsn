from setuptools import setup
from os import path


DESCRIPTION = ""
NAME = "wsn"

CONFIG = {
    "name": NAME,
    "version": "0.1.0",
    "description": DESCRIPTION,
    "packages": ["requirements"],
    "package_data": {"requirements": ["requirements/*"]},
    "include_package_data": True,
    "author": "Marcin Cyc",
    "author_email": "marcinbedcyc@mat.umk.pl",
    "url": "https://gitlab.com/marcinbedcyc/wsn",
    "license": "proprietary",
    "classifiers": [
        "Programming Language :: Python :: 3.8",
        "License :: Other/Proprietary License",
    ],
    "scripts": [
        path.join("zadania", "zadanie1.py"),
        path.join("zadania", "zadanie2.py"),
        path.join("zadania", "zadanie3.py"),
    ]
}

setup(**CONFIG)
