import math
import tkinter as tk
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np

from wsn.data.numbers_data import numbers_9x9, labels_9x9  # , numbers_7x7, labels_7x7,
from wsn.gui.numbers import AdalineGUI
from wsn.neural_networks.adaline import Adaline, fourier_transform


ROWS = 9
COLS = 9


def sigmoida(x):
    return 1.0 / (1.0 + math.exp(-x))


def sigmoida_derivative(x):
    return x * (1.0 - x)


class Adalines10:
    def __init__(self, prediction_confidence_plot=False):
        self.prediction_confidence_plot = prediction_confidence_plot
        self.adalines = []
        for _ in range(10):
            self.adalines.append(Adaline(
                ROWS*COLS,
                iterations=20000,
                error_limit=0.001,
                activation=sigmoida,
                activation_derivative=sigmoida_derivative
            ))

    def learn(self):
        begin = datetime.now()
        print("Begin learning 10 adalines")
        for i in range(10):
            self.adalines[i].train(numbers_9x9, labels_9x9[i], name=str(i), with_normalize=True)
        end = datetime.now()
        print(f"End learning 10 adalines. Duration: {end-begin}")

    def predict(self, input):
        results = [
            self.adalines[i].output(
                np.concatenate([input, fourier_transform(input)])
            ) for i in range(10)
        ]
        argmax = np.argmax(results)
        print(results)
        print(argmax)
        if self.prediction_confidence_plot:
            array10 = np.arange(10)
            plt.bar(array10, results)
            plt.yticks([])
            plt.xticks(array10, array10)
            plt.savefig('plots/prediction.png')
            plt.cla()
        return argmax


def main():
    WITH_CONFIDENCE = True
    app = AdalineGUI(
        neural_network=Adalines10(prediction_confidence_plot=WITH_CONFIDENCE),
        master=tk.Tk(),
        rows=ROWS,
        cols=COLS,
        with_confidence=WITH_CONFIDENCE
    )
    app.mainloop()


if __name__ == "__main__":
    main()
