import json
import numpy as np
from wsn.neural_networks.backpropagation import BackpropagationNeuralNetwork
import matplotlib.pyplot as plt
from wsn.data.robot_points import get_temp_point_and_final, Point


WEIGHTS_FILE_PATH = 'weights/test.json'
STRUCTURE = (2, 10, 10, 2)
ARM_LENGTH = 100


def main():
    with open(WEIGHTS_FILE_PATH) as json_file:
        data = json.load(json_file)
        NN = BackpropagationNeuralNetwork(
            STRUCTURE,
            eta=0.005,
            weights=data['weights'],
            bias=data['bias']
        )

        x = [i for i in range(0, 2*ARM_LENGTH)]
        y = [i for i in range(-2*ARM_LENGTH, 2*ARM_LENGTH)]
        A = np.transpose([np.tile(x, len(y)), np.repeat(y, len(x))])

        distance_diff = []

        for i in range(len(A)):
            alfa, beta = NN.predict([A[i][0]/100, A[i][1]/100], debug=False)
            final, _ = get_temp_point_and_final(
                alfa, beta, Point(0, 0), ARM_LENGTH
            )
            distance_diff.append(-np.sqrt(
                np.power(final.x - A[i][0], 2) + np.power(final.y - A[i][1], 2)
            ))

        plt.scatter(A[:, 0], A[:, 1], c=distance_diff)
        plt.show()


if __name__ == "__main__":
    main()
