import json
import random
from wsn.neural_networks.backpropagation import BackpropagationNeuralNetwork

WEIGHTS_FILE_PATH = 'weights/test.json'
DATA_FILE_PATH = (
    '/home/marcin/Dokumenty/studia/SemestrVII/WSN/lab/wsn/wsn/data/robot_arm_points.json'
)
STRUCTURE = (2, 10, 10, 2)
ITERATIONS = 500000


if __name__ == "__main__":
    NN = BackpropagationNeuralNetwork(STRUCTURE, eta=0.005)

    with open(DATA_FILE_PATH) as data_file:
        data = json.load(data_file)
        inputs = data['inputs']
        outputs = data['outputs']

    for i in range(ITERATIONS):
        index = random.randint(0, len(inputs)-1)
        NN.train(inputs[index], outputs[index])
        if i % (ITERATIONS/100) == 0:
            print(f"Progress {(i/ITERATIONS * 100):1.0f}%", end="\r")

    print(f"Structure: {STRUCTURE}")
    print(outputs[0])
    print(NN.predict(inputs[0]))

    for i in range(len(NN.weights)):
        if not isinstance(NN.weights[i], int):
            NN.weights[i] = NN.weights[i].tolist()
        if not isinstance(NN.bias[i], int):
            NN.bias[i] = NN.bias[i].tolist()
    weights_dict = {'weights': NN.weights, 'bias': NN.bias}
    with open(WEIGHTS_FILE_PATH, 'w') as weights_file:
        json.dump(weights_dict, weights_file)
