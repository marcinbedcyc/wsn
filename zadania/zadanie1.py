import tkinter as tk
import matplotlib.pyplot as plt
from datetime import datetime

from wsn.data.numbers_data import numbers_5x7, labels
from wsn.gui.numbers import PerceptronGUI
from wsn.neural_networks.perceptron import Perceptron


ROWS = 5
COLS = 7


class Perceptrons10:
    def __init__(self, learning_algorithm='pla_train', is_patterns_showed=True):
        self.learning_algorithm = learning_algorithm
        self.is_patterns_showed = is_patterns_showed
        self.perceptrons = []
        for _ in range(10):
            self.perceptrons.append(Perceptron(ROWS*COLS))

    def learn(self):
        begin = datetime.now()
        print(f"Begin learning 10 perceptrons with algorithm: {self.learning_algorithm}")
        for i in range(10):
            getattr(self.perceptrons[i], self.learning_algorithm)(numbers_5x7, labels[i])
        end = datetime.now()
        print(f"End learnig 10 perceptrons. Duration: {end-begin}")
        if self.is_patterns_showed:
            self.show_patterns()

    def predict(self, input):
        return " ".join(
            [str(i) if self.perceptrons[i].predict(input) == 1 else '' for i in range(10)]
        )

    def show_patterns(self):
        print("Showing heatmap patterns for learned numbers")
        _, plots = plt.subplots(2, 5)
        for i, plot in enumerate(plots.flat):
            plot.imshow(self.perceptrons[i].weights[1:].reshape(5, 7))
            plot.set_title(f'Liczba {i}')
        plt.show()


def main():
    app = PerceptronGUI(
        neural_network=Perceptrons10(
            learning_algorithm="pla_train",
            is_patterns_showed=False
        ),
        master=tk.Tk(),
        rows=ROWS,
        cols=COLS
    )
    app.mainloop()


if __name__ == "__main__":
    main()
