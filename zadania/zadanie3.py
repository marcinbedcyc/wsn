import tkinter as tk
from wsn.gui.robot import RobotGUI


def main():
    app = RobotGUI(master=tk.Tk())
    app.mainloop()


if __name__ == "__main__":
    main()
